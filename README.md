# RATIONALE #

Internal project that involves gather technical information about accessing our proxy server. That's a bottleneck that we face actually when we organize a symposium or a scientific meetings inside our building. We try to offer a solution thinking in the worst scenario as possible and at the same time be agnostic to operating systems: our goal is offer solution to MacOS, Windows, Linux and support mobile devices.

### What is this repository for? ###

* Quick summary
    - This repo is the core of another future repo: a mobile application to offer for free to the external public that access our wifi connection(s)
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - _In the making_
* Configuration
    - _In the making_
* Dependencies
    - None. This is a _must_ condition
* How to run tests
    - 
* Deployment instructions
    - Just download our app from Google Play

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/proxy-access/issues?status=new&status=open)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/proxy-access/commits/allhttps://bitbucket.org/imhicihu/XXXXXXXXXXXXXXXX/commits/) section for the current status

### Contribution guidelines ###

* Writing tests
    - _In the making_
* Code review
    - _In the making_
* Other guidelines
    - This is and will be a massive guideline. Lots of data will be gathered. 

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/XXXXXXXXXXXX/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)


### Legal ###

* All trademarks are the property of their respective owners.

### Licence ###

* The content of this project itself is licensed under the {XXXXXXXXXXXXXX}    